#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  1 15:53:31 2021

@author: Ismahene
"""

import numpy as np
# =============================================================================
# WLC
# =============================================================================

def WLC(extension, Lc):
    pl=0.4
    T= 298
    kB = 1.3807e-2  # pNnm/K
  
    kBT=kB*T
    force= np.zeros(len(extension))

    for i in range(len(extension)):
        force[i] = kBT/pl*(1/4*(1-extension[i]/Lc)**(-2)-1/4+extension[i]/Lc)
    return force

def residuals( y, extension, Lc):
    return y - WLC(extension, Lc)
