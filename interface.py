#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 22 15:11:41 2021

@author: Ismahene
"""


import tkinter.messagebox  # to show pop-up windows to the user
from tkinter import * #library needed for the GUI
#from tkinter.filedialog import askopenfilename  #permits to the user to open a file
from tkinter import filedialog
from tkinter import ttk   #needed for treeview
from matplotlib.backends.backend_tkagg import ( FigureCanvasTkAgg,NavigationToolbar2Tk) #matplitlib backend compatible with tkinter 
from matplotlib.figure import Figure # to create figures
import parse_tdms_new as tdms #personal library that parses tdms files and extracts info
from scipy.signal import find_peaks
import os #needed for path
import time #needed to calculate time
import functions as func #personal library that contains smoothing functions
import numpy as np #numpy for matrix & arrays
from scipy.optimize import curve_fit  # For curve fitting (e.g. polynomial, sinusoidal...etc,.)
import parse_jpk as JPK   #personal library that parses JPK files and extracts info
import contact_point as cp  #personal library that finds contact point of retract and approach
import WLC as wlc #personal library that fits the Warm-like Chain model
import LcFc as Lc
import NoiseLevel as NL
import RetrieveFiles as RF
import pandas as pd
import export_parms as xp
import FJC as fjc

class App:
    def __init__(self):
        
       # self.widget_main= None
       # self.toolbar_main= None 
        #self.widget_second_tab= None
        #self.toolbar_second_tab=None        global saved_directory

        self.dict_info= { }
        self.dict_raw= { }
        self.window = Tk() 
        self.style = ttk.Style(self.window)
        self.style.configure("lefttab.TNotebook", tabposition="nw")
        self.notebook = ttk.Notebook(self.window, style="lefttab.TNotebook")


        # Get screen size
        screen_width = self.window.winfo_screenwidth()
        screen_height = self.window.winfo_screenheight()
        #self.window.geometry("%dx%d" % (screen_width/2, screen_height/2))
        #self.window.resizable(height = screen_width,  width = screen_height)
    #   self.window.resizable(width=True, height=False)

        

        # Get percentage of screen size from Base size
        percentage_width = screen_width / (600/ 100)
        percentage_height = screen_height / (200/ 100)
        
        # Make a scaling factor, this is bases on average percentage from
        # width and height.
        scale_factor = ((percentage_width + percentage_height) / 2) / 100
        
        # Set the fontsize based on scale_factor,
        # if the fontsize is less than minimum_size
        # it is set to the minimum size
        fontsize = int(14 * scale_factor)
        minimum_size = 8
        if fontsize < minimum_size:
            fontsize = minimum_size
        #creation of frames
        self.main_tab = Frame(self.notebook,  width=screen_width*scale_factor, height=screen_height*scale_factor)


        #add notebook
        self.notebook.add(self.main_tab, text="Main")

        # initialization 
        self.frame=Frame(self.main_tab)
        
        #pack notebook 
        self.notebook.pack(side='top')
        
        #window title
        self.window.title("AFM_FS software")
        #window size
        self.window.minsize(800, 400)
        self.window.config()

# =============================================================================
# Add subframes of main tab (subframes are needed to embedd plots and buttons 
# in the same window)
# =============================================================================
        self.subframe_main_tab= Frame(self.frame)
        self.subframe_plots= Frame(self.frame)
        
        #creation of treeview to  visualize detected peak detection 

        self.subframe_tab_peaks= Frame(self.frame)
        self.subframe_list_peaks= Frame(self.frame)

        
        self.columns=('peak index', 'Indentation/Separation (nm)', 'Force [pN]' , 'Loading Rate (pN/s)')
        self.scrollbar=Scrollbar(self.subframe_tab_peaks)
        self.tab_peak_detection = ttk.Treeview(self.subframe_tab_peaks,columns=self.columns,selectmode="extended",yscrollcommand=self.scrollbar.set)

        #defenition of headers and size of columns of peak detection tab
        self.tab_peak_detection.heading("peak index", text="peak index")
        self.tab_peak_detection.column("peak index")
        self.tab_peak_detection.heading("Indentation/Separation (nm)", text="Indentation/Separation (nm)")
        self.tab_peak_detection.column("Indentation/Separation (nm)")
        self.tab_peak_detection.heading("Force [pN]", text="force [pN]")
        self.tab_peak_detection.column("Force [pN]")
        self.tab_peak_detection.heading("Loading Rate (pN/s)", text="Loading Rate (pN/s)")
        self.tab_peak_detection.column("Loading Rate (pN/s)")
        self.scrollbar.config(command=self.tab_peak_detection.yview)


        self.frame.pack(expand=YES)
        self.subframe_main_tab.grid(row=0, column =1)
        self.subframe_plots.grid(row=0, column=0)
        self.subframe_tab_peaks.grid(row=1, column=0)
        self.subframe_list_peaks.grid(row=1, column=1)




        # creation of composants
        self.create_widgets()
        
    def create_widgets(self):
        """
        Loads all the widgets of the app
        
        Returns
        -------
        None.
        
        """
        self.create_buttons_main_tab()
        
    def create_buttons_main_tab(self):
        """
        This function creates all the buttons and entries of the main tab

        Returns
        -------
        None.

        """
        upload_button = Button(self.subframe_main_tab, text=" Upload file", font=("Courrier", 10)
                          ,width=30, command=self.fileDialog)
        upload_button.grid(row=0,column=1)
        
        self.combo_file = ttk.Combobox(self.subframe_main_tab, 
                            values=[
                                    ".tdms", 
                                    ".jpk"
                                    ], font=("Courrier", 10),width=5)
        self.combo_file.grid(row=0, column= 2)
        self.combo_file.current(0)
        
        label_file_name= Label(self.subframe_main_tab,  text=" or set file name", font=("Courrier", 10),width=15)
        label_file_name.grid(row=2, column= 1, sticky=W)
        
        self.file_name= StringVar()
        entry_file_name= Entry(self.subframe_main_tab, textvariable= self.file_name,  font=("Courrier", 10),width=20)
        entry_file_name.grid(row=2, column= 1, sticky=E)
       # entry_file_name.insert('end', '*')

    
    def get_file_from_retrieve(self):
        """
        Gets the files from the name given by the user 

        Returns
        -------
        None.

        """
        if self.file_name.get() and  self.combo_file.get() == ".tdms":
            fnMask= self.file_name.get() + str('*')
            self.path= RF.TdmsfileNamesRetrieve(fnMask)
            if (os.path.sep) == "\\" :
                self.directory = self.path.split('\\')[-2]

            else:
                self.directory= self.path.split('/')[-2]

            self.path, self.all_tdms= tdms.grab_tdms(self.directory)
            self.tdms_file=self.path.split('/')
                        #add a label with the name of the file
            self.label_tdms= Label(self.subframe_main_tab, text=str(self.tdms_file[-1]),font=("Courrier", 10),width=30)
            self.label_tdms.grid(row=1, column=1)


    def fileDialog(self):
        """
        
        This function opens a window to select the tdms or JPK files 
        -------
        Returns
        -------
        None.
        """

        self.path=None
        self.get_file_from_retrieve()

        if self.combo_file.get() == ".tdms" and self.path == None:
           
            #self.path=askopenfilename(title = "Select A File",
             #                                 filetypes =(("tdms File", "*.tdms"),("tdms", "*.tdms"),("tdms","*.tdms*")) ) 
            self.directory = filedialog.askdirectory( initialdir=".")
            #self.path=self.directory
            #if (os.path.sep) == "\\" :
            #  self.directory = self.path.split('\\')


            # else:
            # self.directory= self.path.split('/')


            self.path, self.all_tdms= tdms.grab_tdms(self.directory)
            if self.path ==None:
                tkinter.messagebox.showinfo("Warning ", "No tdms file found !!")
            self.tdms_file=self.path.split('/')
            #add a label with the name of the file
            self.label_tdms= Label(self.subframe_main_tab, text=str(self.tdms_file[-1]),font=("Courrier", 10),width=30)
            self.label_tdms.grid(row=1, column=1)
            #save the name of the directory in a variable, this one is needed to load the text file of parameters
            #try:
            # self.directory= '/'.join(map(str, self.tdms_file[:-1]))
            # except:
            #  tkinter.messagebox.showinfo("Warning ", "Please upload a file")
                
        
        elif self.combo_file.get() == ".jpk":
           # self.path=askopenfilename( title = "Select A File",
            #                                  filetypes =(("jpk File", "*.jpk"),("jpk-force", "*.jpk-force"),("jpk","*.jpk*")) ) 
            self.directory = filedialog.askdirectory( initialdir=".")
            self.path= JPK.grab_jpk(self.directory)
            if self.path==None:
                tkinter.messagebox.showinfo("Warning ", "No JPK file found !!")

            self.jpk_file=self.path.split('/')
            self.label_jpk= Label(self.subframe_main_tab, text=str(self.jpk_file[-1]),font=("Courrier", 10),width=30)
            self.label_jpk.grid(row=1, column=1)
            
            try:
                self.jpk_file[-2]
                self.directory= '/'.join(map(str, self.jpk_file[:-1]))
               # self.directory= self.tdms_file[-2]
            except:
                tkinter.messagebox.showinfo("Warning ", "Please upload a file")

        
        # plot the graph on the GUI
        self.get_deflection_vs_piezo()

        #create and pack 'next' button to be able to navigate through the files
        next_button= Button (self.subframe_main_tab, text= "Next",  font=("Courrier", 10) , width=5, command= self.nextFile)
        next_button.grid(row=3,column=1, sticky= E)
        
        #previous button
        previous_button= Button (self.subframe_main_tab, text= "Previous",  font=("Courrier", 10), width=5, command= self.PreviousFile)
        previous_button.grid(row=3,column=1, sticky= W)
        
        # prepare checkbuttons and variables ( the user can choose to see approach and retract seperated or not )
        self.approachOn=IntVar()
        approach_checkbutton= Checkbutton(self.subframe_main_tab, text= "approach", variable=self.approachOn, font=("Courrier", 10), onvalue = 1, offvalue = 0)
        #approahc is selected by default
        approach_checkbutton.select()
        approach_checkbutton.grid(row=7, column=2)
        
        self.retractOn= IntVar()
        retract_checkbutton= Checkbutton(self.subframe_main_tab, text= "retract", variable=self.retractOn, font=("Courrier", 10),onvalue = 1, offvalue = 0)
        #retract is selected by default
        retract_checkbutton.select()
        retract_checkbutton.grid(row=8, column=2)
        

        
        if self.combo_file.get() == ".tdms":
            #loading the parameters from the txt file of the parameters in the same folder as the force curves 
            
            self.distance, self.force, self.K, self.invOLS, self.sensitivity, self.piezo_gain , self.index_end_approach, self.index_start_approach, self.index_start_retract, self.index_end_retract, self.dwell, self.time= tdms.GetForceDistAndParms(self.directory, self.channel_data_deflection, self.channel_data_piezo, self.time)
            #compute the extension 
            self.extension= tdms.ComputeExtension(self.force, self.distance, self.K)
            self.channel_data_deflection_nm= tdms.DeflectionInNanometer(self.channel_data_deflection , self.invOLS)
            self.dict_info["Deflection (nm)"]= self.channel_data_deflection_nm
            self.dict_raw["Deflection (nm)"]= self.channel_data_deflection_nm


            
            # offset and save the extension in a dictionary that contains all important info
            self.dict_info["Indentation/Separation (nm)"]= self.extension + max(self.extension)
            #print(self.dict_info["Indentation/Separation (nm)"][0:10])
            self.dict_raw["Indentation/Separation (nm)"]= self.extension + max(self.extension)
            # offset and save the distance in nm in the dictionnary of info
            self.dict_info["Distance (nm)"]= self.distance+ max(self.distance)
            self.dict_raw["Distance (nm)"]= self.distance+ max(self.distance)
            # save the force in pN in the dictionnary of info
            self.dict_info["Force (pN)"]= self.force
            self.dict_raw["Force (pN)"]= self.force
            # save the index of the start of the approach
            self.dict_info["index start approach"]= self.index_start_approach
            # save the inddex of the end of the approach
            self.dict_info["index end approach"]= self.index_end_approach
            # save the index of the start of the retract
            self.dict_info["index start retract"]= self.index_start_retract
            # save the index of the end of the retract 
            self.dict_info["index end retract"]= self.index_end_retract
            self.dict_info["Spring constant (N/m)"]= self.K


        # if it is a JPK file, the information is loaded differently from tdms
        elif self.combo_file.get() == ".jpk":
            self.force, self.time, self.distance, self.extension, self.parameters, self.index_start_approach, self.index_end_approach, self.index_start_retract, self.index_end_retract= JPK.parse_jpk(self.path)
            #save all necessary info in dico of info
            self.dict_info["Indentation/Separation (nm)"]= self.extension
            self.dict_info["Distance (nm)"]= self.distance
            self.dict_info["Force (pN)"]= self.force
            self.dict_info["index start approach"]= self.index_start_approach
            self.dict_info["index end approach"]= self.index_end_approach
            self.dict_info["index start retract"]= self.index_start_retract
            self.dict_info["index end retract"]= self.index_end_retract
            self.dict_info['time (ms)'] =self.time
            self.K= self.parameters['spring constant']
            self.sensitivity= self.parameters['sensitivity']
            self.invOLS= None


        # create entry to indicate the RMS
        self.percentage_noise= IntVar()
        entry_percentage_noise=Entry(self.subframe_main_tab, textvariable= self.percentage_noise,  font=("Courrier", 10),width=10)
        entry_percentage_noise.delete(0, 'end')
        entry_percentage_noise.insert(0, 30)
        entry_percentage_noise.grid(row=4, column=1, sticky= E)

       # self.entry_noise= Entry(self.subframe_main_tab,   font=("Courrier", 10),width=5)
            #calculate RMS
        self.RMS= NL.get_RMS(self.dict_info["Force (pN)"][ self.dict_info["index start retract"]: self.dict_info["index end retract"]], 1)  
      #  self.entry_noise.delete(0, 'end')
      #  self.entry_noise.insert(0, str(self.RMS))
       # self.entry_noise.grid(row=4, column=1,sticky=E)

        self.label_noise= Label(self.subframe_main_tab, text= "RMS:     "+str(self.RMS)+ "  pN"+ "      Set %  here: ",  font=("Courrier", 10) )
        
        self.label_noise.grid(row= 4, column=1, sticky=W)
        
        button_noise= Button(self.subframe_main_tab,  text= "Get RMS",  font=("Courrier", 10), width=5, command= self.noise_level)
        button_noise.grid(row=4, column=2 )
        
        # spring constant
        self.entry_k= Entry(self.subframe_main_tab,   font=("Courrier", 10),width=15)
        self.entry_k.insert(0, str(self.K))
        self.entry_k.grid(row=5, column=1, sticky=E)
        label_k= Label(self.subframe_main_tab, text= "Spring constant"+'\n'+ "(N/m)",  font=("Courrier", 9) )
        label_k.grid(row= 5, column=1, sticky=W)
        
        # if the user wants to change the spring constant
        self.change_K= IntVar()
        change_k_checkbutton= Checkbutton(self.subframe_main_tab, variable= self.change_K,   text= "Change it",  font=("Courrier", 10), width=10, onvalue=1, offvalue=0)
                                 
        change_k_checkbutton.grid(row=5, column=2 )
        
        #invOLS entry
        self.entry_invols= Entry(self.subframe_main_tab,   font=("Courrier", 10),width=15)
        self.entry_invols.insert(0, str(self.invOLS))
        self.entry_invols.grid(row=6, column=1, sticky=E)
        label_invols= Label(self.subframe_main_tab, text= "invOLS (nm/V)",  font=("Courrier", 10) )
        label_invols.grid(row= 6, column=1, sticky=W)
        
        # if the user wants to change the invols
        self.change_invOLS= IntVar()
        change_invols_checkbutton= Checkbutton(self.subframe_main_tab, variable=self.change_invOLS ,text= "Change it",  font=("Courrier", 10), width=10 ,onvalue=1, offvalue=0)
                                   
        change_invols_checkbutton.grid(row=6, column=2 )

        
        ## if the user wants to change the invols
        #change_invols_button= Button(self.subframe_main_tab,  text= "OK",  font=("Courrier", 10), width=5,
         #                            command= self.change_parameters)
        #change_invols_button.grid(row=6, column=2, sticky= E )

        #combobox containing plotting options on the x-axis
        self.combo_xaxis = ttk.Combobox(self.subframe_main_tab, 
                            values=[
                                    "Piezo (V)", 
                                    "Indentation/Separation (nm)",
                                    "time (ms)",
                                    "Distance (nm)"
                                    ], font=("Courrier", 10),width=30)
        self.combo_xaxis.grid(row=7, column= 1)
        self.combo_xaxis.current(1)
        

        # combobox containing plotting options on the y-axis
        self.combo_yaxis = ttk.Combobox(self.subframe_main_tab, 
                            values=[
                                    "Deflection (V)", 
                                    "Deflection (nm)", 
                                    "Force (pN)"
                                    ],font=("Courrier", 10),width=30)
        self.combo_yaxis.grid(row=8, column= 1)
        self.combo_yaxis.current(2)
        

        # once parameters selected from combobox x-axis and y-axis the user can press see curve
        plot_button= Button(self.subframe_main_tab, text= "See curve",  font=("Courrier", 10)
                             , width=5, command= self.get_combo_values)
        plot_button.grid(row=9,column=1)

        
# =============================================================================
#    •	Savitzky-Golay filter buttons and entries 
# =============================================================================
        self.smoothing_window= IntVar()
        entry_smoothing=Entry(self.subframe_main_tab, textvariable=self.smoothing_window, width=15, font=("Courrier", 10))  
        entry_smoothing.grid(row=10, column=1, sticky= E)
        
        label_smoothing=Label(self.subframe_main_tab, text='Savizky-Golay '+'\n'+'(odd number)'+'\n'+'> 3', font=("Courrier", 10))  
        label_smoothing.grid(row=10, column=1, sticky= W )
        
        self.SmoothingOn= IntVar()
        smooth_checkbutton= Checkbutton(self.subframe_main_tab, text= "Apply filter",variable=self.SmoothingOn,  font=("Courrier", 12), onvalue = 1, offvalue = 0
                             , width=10)
        smooth_checkbutton.grid(row=11,column=1)
        
        
# =============================================================================
# buttons and entries to Correct the virtual deflection
# =============================================================================
        label_correction= Label(self.subframe_main_tab, text= "Correct virtual deflection")
        label_correction.grid(row=12, column= 1)

        # if the user wants to correct the deflection only from the approach curve
        self.FromApp=IntVar()
        correction_from_approach_button= Checkbutton(self.subframe_main_tab, text= "from approach", variable=self.FromApp, font=("Courrier", 10), onvalue = 1, offvalue = 0)
        #correction_from_approach_button.select()
        correction_from_approach_button.grid(row=13, column=1)
        
        # entry of the order of the polynomial fitting that the user needs to indicate
        self.Npoly= IntVar()
        entry_Npoly= Entry(self.subframe_main_tab, textvariable=self.Npoly, font=("Courrier", 10), width=15) 
        #delete default value ( zero is default value given by tkinter)
        entry_Npoly.delete(0, 'end')
        #insert polynomial order 1
        entry_Npoly.insert(0, 1)
        entry_Npoly.grid(row=14, column= 1 ,sticky= E)
         
        #label of polynomial order 
        label_Npoly= Label(self.subframe_main_tab, text=" N poly", font=("Courrier", 10)) 
        label_Npoly.grid(row=14, column= 1 ,sticky= W)
        
        # the user can correct the deflection from both retract and approach by setting the % of each curve
        self.pourcentage_retract= IntVar()
        entry_retract_pourcentage= Entry(self.subframe_main_tab, textvariable=self.pourcentage_retract, font=("Courrier", 10)) 
        entry_retract_pourcentage.insert(0, 3)
        entry_retract_pourcentage.grid(row=15, column= 1 ,sticky= E)
        
        #label % retract
        label_retract_pourcentage= Label(self.subframe_main_tab, text=" % retract", font=("Courrier", 10)) 
        label_retract_pourcentage.grid(row=15, column= 1 ,sticky= W)
        # entry of the % of the approach curve
        self.pourcentage_approach= IntVar()
        entry_approach_pourcentage= Entry(self.subframe_main_tab, textvariable=self.pourcentage_approach, font=("Courrier", 10)) 
        entry_approach_pourcentage.insert(0, 7)
        entry_approach_pourcentage.grid(row=16, column= 1 ,sticky= E)
        # label of the percentage from the approach
        label_approach_pourcentage= Label(self.subframe_main_tab, text=" % approach", font=("Courrier", 10)) 
        label_approach_pourcentage.grid(row=16, column= 1 ,sticky= W)
        # button to press after all parms are selected to get the deflection corrected
        self.CorrectVirtualDeflection = IntVar()
        correct_deflection_checkbutton= Checkbutton(self.subframe_main_tab, text= "Correct deflection",  variable= self.CorrectVirtualDeflection, font=("Courrier", 10), onvalue=1, offvalue=0) 
        correct_deflection_checkbutton.select()
        correct_deflection_checkbutton.grid(row=17,column=1, sticky= W)
        
        
        apply_deflection_correction_button= Button(self.subframe_main_tab, text= "Apply ",  font=("Courrier", 10), width=5,
                              command=self.apply_virtual_deflection_correction)
        apply_deflection_correction_button.grid(row=19,column=1, sticky=E)#avant row=17
        
        
        #automatic correction of virtual deflection
        #button of contact point of retract 
        self.CpRetractOn=IntVar()
        contact_point_retract_checkbutton= Checkbutton(self.subframe_main_tab, text= "Contact Point"+'\n'+"retract", variable=self.CpRetractOn, font=("Courrier", 10), onvalue = 1, offvalue = 0)
        #approach is selected by default
        contact_point_retract_checkbutton.select()
        #show CP of retract
        
        #contact_point_retract_button= Button(self.subframe_main_tab,  text= "Contact Point"+'\n'+"retract",  font=("Courrier", 10), 
         #                          command=self.show_point_contact_retract )
        contact_point_retract_checkbutton.grid(row=18, column=1 , sticky=E)
        #button of contact point of approach
        self.CpApproachOn=IntVar()
        contact_point_approach_checkbutton= Checkbutton(self.subframe_main_tab, text= "Contact Point"+'\n'+"approach", variable=self.CpApproachOn, font=("Courrier", 10), onvalue = 1, offvalue = 0)
        contact_point_approach_checkbutton.select()

        contact_point_approach_checkbutton.grid(row=18, column=1, sticky= W)
        #show CP of approach
        # offset the contact point to 0 on the x-axis
        self.CorrectCP=IntVar()
        #correct_contact_point_button= Button(self.subframe_main_tab,  text= "Correct Contact Point ",   font=("Courrier", 10), 
         #                                    command= self.correct_point_contact)
        #correct_contact_point_button.grid(row=19, column=1)
        correct_contact_point_checkbutton= Checkbutton(self.subframe_main_tab,  text= "Correct Contact Point ",  variable= self.CorrectCP, font=("Courrier", 10),  onvalue = 1, offvalue = 0)
        correct_contact_point_checkbutton.select()
        correct_contact_point_checkbutton.grid(row=19, column=1, sticky= W)
        
        # apply_corrections_button= Button(self.subframe_main_tab,  text= "Apply ",   font=("Courrier", 10), width=5,
        #                                      command= self.apply_corrections)
        # apply_corrections_button.grid(row=19, column=1, sticky= E)


        
        #correct contact point of approach and retract
        #plots the options selected on the combobox once the file is loaded
        self.get_combo_values()
        # int variable that contains the number of peaks given by the user
        self.N_peaks =IntVar()
        # int variable that contains the width value given by the user before the peak detection
        self.width= IntVar()
        
        self.prominence= IntVar()
        # labels for both number of peaks and width
        label_number_peaks= Label(self.subframe_main_tab, text= 'Number '+ '\n'+'of'+'\n'+'peaks', font=("Courrier", 10)) 
        label_number_peaks.grid(row=11, column= 2, sticky=W)
        
        label_width= Label(self.subframe_main_tab, text= 'Width', font=("Courrier", 10)) 
        label_width.grid(row=10, column= 2, sticky=W)
        
        width_entry= Entry(self.subframe_main_tab, textvariable= self.width,  font=("Courrier", 10), width=5)
        width_entry.grid(row= 10, column=2, sticky=E)
        width_entry.delete(0, END)
        width_entry.insert(0, 3)
        
        label_prominence= Label(self.subframe_main_tab, text= 'Promi-'+ '\n'+'nence', font=("Courrier", 10)) 
        label_prominence.grid(row=9, column= 2, sticky=W)
        
        prominence_entry= Entry(self.subframe_main_tab, textvariable= self.prominence,  font=("Courrier", 10), width=5)
        prominence_entry.grid(row= 9, column=2, sticky=E)
        
        #button for peak detection
        self.DetectPeaksOn= IntVar()
        peak_detection_checkbutton= Checkbutton(self.subframe_main_tab, text= "Detect peaks", variable=self.DetectPeaksOn,  font=("Courrier", 10), onvalue=1, offvalue=0
                             , width=12)
        peak_detection_checkbutton.grid(row=12,column=2)
        # entry to set the number of peaks 
        npeaks_entry= Entry(self.subframe_main_tab, textvariable= self.N_peaks,  font=("Courrier", 10), width=5)
        npeaks_entry.grid(row= 11, column=2, sticky=E)
        npeaks_entry.delete(0, END)
        npeaks_entry.insert(0, 1)
        
        # float variable to save the persistence length
        self.range_before_peak= IntVar()
        # string variable to save the contour length
        self.contour_length= StringVar()
        #label for the persistence length
        pl_label= Label(self.subframe_main_tab, text= 'Range '+ '\n'+ '(nm)', font=("Courrier", 10)) 
        pl_label.grid(row=13, column= 2, sticky=E)
        # label to introduce the contour length
        lc_label= Label(self.subframe_main_tab, text= 'Contour '+ '\n'+'length', font=("Courrier", 10)) 
        lc_label.grid(row=13, column= 2, sticky=W)
        
        # button to fit WLC model
        #self.WLCOn= IntVar()
        #fit_WLC_checkbutton= Checkbutton(self.subframe_main_tab, text= "Fit WLC",  variable= self.WLCOn, font=("Courrier", 10), onvalue=1, offvalue=0,
         #                     width=8)
        #fit_WLC_checkbutton.grid(row=14,column=2)
        
        self.combo_fit_model = ttk.Combobox(self.subframe_main_tab, 
                            values=[
                                    "fit WLC", 
                                    "fit FJC",
                                    ], font=("Courrier", 10),width=5)
        self.combo_fit_model.grid(row=15,column=2)
        #self.combo_fit_model.current(0)
        
        
        
        # persistance length entry (here the user sets the value)
        range_before_peak_entry= Entry(self.subframe_main_tab, textvariable= self.range_before_peak,  font=("Courrier", 10), width=5)
        range_before_peak_entry.grid(row= 14, column=2, sticky=E)
        range_before_peak_entry.delete(0, END)
        range_before_peak_entry.insert(0, 3)
        # contour length entry (here the user sets the value)
        self.lc_entry= Entry(self.subframe_main_tab, textvariable= self.contour_length,  font=("Courrier", 10), width=5)
        self.lc_entry.grid(row= 14, column=2, sticky=W)
        
        FcLc_button= Button(self.subframe_main_tab, text= "Plot Fc vs Lc"+'\n'+"& detect peaks",  font=("Courrier", 10)
                             , width=8, command= self.plot_FcLc)
        FcLc_button.grid(row=16,column=2)

        
        self.force_threashold= IntVar()
        force_threshold_entry= Entry(self.subframe_main_tab, textvariable= self.force_threashold,  font=("Courrier", 10), width=5)
        force_threshold_entry.grid(row= 18, column=2)
        
        Lc_peak_button= Button(self.subframe_main_tab, text= "Superpose peaks"+ '\n'+"on F-dis",  font=("Courrier", 10)
                             , width=8, command= self.plot_peak_FcLc)
        Lc_peak_button.grid(row=18,column=2)
        
        
        self.threshold= DoubleVar()
        entry_threshold=Entry(self.subframe_main_tab, textvariable=self.threshold, width=4, font=("Courrier", 10))  
        entry_threshold.grid(row= 17, column =2, sticky= E)
        
        threshold_label= Label(self.subframe_main_tab, text= 'Force '+ '\n'+'threshold '+ '\n'+ '(pN)', font=("Courrier", 9), width=6) 
        threshold_label.grid(row=17, column= 2, sticky=W)
        
        
        loading_rate_button= Button(self.subframe_main_tab, text= "Show"+ '\n'+ "loading rates",  font=("Courrier", 10)
                             , width=8, command= self.plot_loading_rate)
        loading_rate_button.grid(row=19,column=2)
        
        export_data_button= Button(self.subframe_main_tab, text= "export data",  font=("Courrier", 10)
                             , width=8, command= self.export_data)
        export_data_button.grid(row=20,column=2)
        
        self.hysteresis= IntVar()
        entry_hysteresis=Entry(self.subframe_main_tab, textvariable=self.hysteresis, width=5, font=("Courrier", 10))  
        entry_hysteresis.delete(0, END)
        entry_hysteresis.insert(0, 35)
        entry_hysteresis.grid(row=3, column=2, sticky=S)
        
       # label_hysteresis=Label(self.subframe_main_tab, text='Hysteresis', font=("Courrier", 10), width=10)  
        #label_hysteresis.grid(row=2, column=2, sticky= N)
                
        hysteresis_label= Label(self.subframe_main_tab, text= "Hysteresis",  font=("Courrier", 10)
                             , width=10)
        hysteresis_label.grid(row=2,column=2)
        
        #activate jump to next curve option
        self.JumpOn= IntVar()
        jump_curve_checkbutton= Checkbutton(self.subframe_main_tab, text= "Jump to next", variable=self.JumpOn,  font=("Courrier", 10), onvalue=1, offvalue=0
                             , width=12)
        jump_curve_checkbutton.grid(row=1,column=2)
        
        self.delete_button= Button(self.subframe_tab_peaks, text= "Delete selected peaks ",  font=("Courrier", 12)
                             , width=15, command= self.delete_peaks)
        

        
        self.apply_virtual_deflection_correction()
        self.show_point_contact_approach()
        self.show_point_contact_retract()
        self.correct_point_contact()
        
    def nextFile(self):
        """
        When the button 'Next' is pressed, this function is called
        it navigates through the files of directory
        and plot deflection vs piezo

        Returns
        -------
        None.

        """
        # if the user wants to see the next file, the dico containing info of the previous file is cleaned
        self.dict_info.clear()
        self.dict_raw.clear()
        xaxis=  str(self.combo_xaxis.get())
        if self.combo_file.get() == ".tdms":
            if (os.path.sep) == "\\" :
                next_index = self.all_tdms.index(self.directory+ '\\' +self.tdms_file[-1].split( "\\")[-1] ) + 1
            else:
                next_index = self.all_tdms.index(self.directory + '/' + self.tdms_file[-1]) + 1

            # get the index of the next file on the list
            # next_index= self.all_tdms.index(self.directory+'/'+self.tdms_file[-1])+1
            # if the user gets to the end of the list: go back to first element (index 0)
            if  next_index == len(self.all_tdms):
                next_index=0
            next_file= self.all_tdms[next_index]
            #get whole path to the selected file
            self.path=  os.path.abspath(self.all_tdms[next_index])
            # get the name of the file
            self.tdms_file= next_file.split('/')
            # show the name of the file on the label of the GUI
            self.label_tdms.config(text=self.tdms_file[-1] )
            # extract deflection and piezo from file and plot them
            self.get_deflection_vs_piezo()
            # extract all parms, force, distance...etc.
            self.distance, self.force, self.K, self.invOLS, self.sensitivity, self.piezo_gain, self.index_end_approach, self.index_start_approach, self.index_start_retract, self.index_end_retract, self.dwell, self.time= tdms.GetForceDistAndParms(self.directory, self.channel_data_deflection, self.channel_data_piezo,self.time)
            # compute the extension
            self.extension= tdms.ComputeExtension(self.force, self.distance, self.K)
            self.channel_data_deflection_nm= tdms.DeflectionInNanometer(self.channel_data_deflection , self.invOLS)

            # save all info the dico 
            self.dict_info["Indentation/Separation (nm)"]= self.extension + max(self.extension)
            self.dict_raw["Indentation/Separation (nm)"]= self.extension + max(self.extension)

            self.dict_info["Distance (nm)"]= self.distance+ max(self.distance)
            self.dict_raw["Distance (nm)"]= self.distance+ max(self.distance)

            self.dict_info["Force (pN)"]= self.force
            self.dict_raw["Force (pN)"]= self.force

            self.dict_info["Deflection (nm)"]= self.channel_data_deflection_nm
            self.dict_raw["Deflection (nm)"]= self.channel_data_deflection_nm

            self.dict_info["index start approach"]= self.index_start_approach
            self.dict_info["index end approach"]= self.index_end_approach
            self.dict_info["index start retract"]= self.index_start_retract
            self.dict_info["index end retract"]= self.index_end_retract
            self.dict_info["Spring constant (N/m)"]=self.K 
            if xaxis == "time (ms)":
                self.show_point_contact_retract()
                self.correct_point_contact()
                self.apply_virtual_deflection_correction()
                self.noise_level()
            else:
                self.show_point_contact_approach()
                self.show_point_contact_retract()
                self.correct_point_contact()
                self.apply_virtual_deflection_correction()
                self.noise_level()
            
        # if the user selects JPK
        elif self.combo_file.get() == ".jpk":
            # get all the jpk files of the directory
            all_jpk_files= JPK.GetJPKFiles(self.directory)
            # save the nme of the jpk file after splitting
            self.jpk_file= self.path.split('/')
            # get the index of the next JPK file
            next_index= all_jpk_files.index(self.directory+'/'+self.jpk_file[-1])+1
            if  next_index == len(all_jpk_files):
                next_index=0
            next_file= all_jpk_files[next_index]
            self.path=  os.path.abspath(all_jpk_files[next_index])
            #save the name of JPK file after splitting
            self.jpk_file= self.path.split('/')
            # insert the name in the label
            self.label_jpk.config(text=self.jpk_file[-1] )
            # get the deflection and the piezo
            self.get_deflection_vs_piezo()
            # extract parms, time, force...etc,.
            self.force, self.time, self.distance, self.extension, self.parameters, self.index_start_approach, self.index_end_approach, self.index_start_retract, self.index_end_retract= JPK.parse_jpk(self.path)
            # save all the info in the dico of info
            self.dict_info["Indentation/Separation (nm)"]= self.extension
            self.dict_info["Distance (nm)"]= self.distance
            self.dict_info["Force (pN)"]= self.force
            self.dict_info["index start approach"]= self.index_start_approach
            self.dict_info["index end approach"]= self.index_end_approach
            self.dict_info["index start retract"]= self.index_start_retract
            self.dict_info["index end retract"]= self.index_end_retract
            self.dict_info['time (ms)'] =self.time
            self.K= self.parameters['spring constant']
            self.sensitivity= self.parameters['sensitivity']
        # plot according to what the user have selected from x-axis and y-axis in the combobox
        #self.get_combo_values()
            
        
    def PreviousFile(self):
        """
        When the button 'previous' is pressed, this function is called
        it navigates through the files of directory
        and plot deflection vs piezo

        Returns
        -------
        None.

        """
        # if 'previous' button is pressed: the dico of info is cleaned 
        self.dict_info.clear()
        self.dict_raw.clear()
        if self.combo_file.get() == ".tdms":
            if (os.path.sep) == "\\" :
                prev_index = self.all_tdms.index(self.directory+ '\\' +self.tdms_file[-1].split( "\\")[-1] ) - 1
            else:
                prev_index = self.all_tdms.index(self.directory + '/' + self.tdms_file[-1]) - 1

            # get the index of the previous file
            # prev_index= self.all_tdms.index(self.directory+'/'+self.tdms_file[-1])-1
            # if the index is 0: go back to the end of the list to be able to navigate 
            if  prev_index == -1:
                prev_index=len(self.all_tdms) -1
            prev_file= self.all_tdms[prev_index]
            self.path=  os.path.abspath(prev_file)

            self.tdms_file= self.path.split('/')
            #insert the name in the label on the GUI
            self.label_tdms.config(text=self.tdms_file[-1] )
            
            # get the deflection and the piezo
            self.get_deflection_vs_piezo()
            # get parms, force, distance...etc.,
            self.distance, self.force, self.K, self.invOLS, self.sensitivity, self.piezo_gain, self.index_end_approach, self.index_start_approach, self.index_start_retract, self.index_end_retract, self.dwell, self.time = tdms.GetForceDistAndParms(self.directory, self.channel_data_deflection, self.channel_data_piezo, self.time)
            # compute the extension
            self.extension= tdms.ComputeExtension(self.force, self.distance, self.K)
            self.channel_data_deflection_nm= tdms.DeflectionInNanometer(self.channel_data_deflection , self.invOLS)
            self.dict_info["Deflection (nm)"]= self.channel_data_deflection_nm
            self.dict_raw["Deflection (nm)"]= self.channel_data_deflection_nm

            
            # save all info in the dictionary
            self.dict_info["Indentation/Separation (nm)"]= self.extension + max(self.extension)
            self.dict_raw["Indentation/Separation (nm)"]= self.extension + max(self.extension)

            self.dict_info["Distance (nm)"]= self.distance
            self.dict_raw["Distance (nm)"]= self.distance

            self.dict_info["Force (pN)"]= self.force
            self.dict_raw["Force (pN)"]= self.force

            self.dict_info["index start approach"]= self.index_start_approach
            self.dict_info["index end approach"]= self.index_end_approach
            self.dict_info["index start retract"]= self.index_start_retract
            self.dict_info["index end retract"]= self.index_end_retract
            self.dict_info["Spring constant (N/m)"]=self.K

            self.show_point_contact_approach()
            self.show_point_contact_retract()
            self.correct_point_contact()
            self.apply_virtual_deflection_correction()    
            self.noise_level()
            
            
        # if the user wants to load a JPK file
        elif self.combo_file.get() == ".jpk":
            all_jpk_files= tdms.GetJPKFiles(self.directory)
            self.jpk_file= self.path.split('/')
            #get index of the pevious file
            prev_index= all_jpk_files.index(self.directory+'/'+self.jpk_file[-1])-1
            if  prev_index == 0:
                prev_index=len(all_jpk_files)
            prev_file= all_jpk_files[prev_index]
            self.path=  os.path.abspath(all_jpk_files[prev_index])
            self.jpk_file= self.path.split('/')
            #insert the name of the file on the GUI
            self.label_jpk.config(text=self.jpk_file[-1] )
            # get the deflection vs piezo
            self.get_deflection_vs_piezo()
            #extract parms, force, time...etc
            self.force, self.time, self.distance, self.extension, self.parameters, self.index_start_approach, self.index_end_approach, self.index_start_retract, self.index_end_retract= JPK.parse_jpk(self.path)
            # save all info in dico of info
            self.dict_info["Indentation/Separation (nm)"]= self.extension
            self.dict_info["Distance (nm)"]= self.distance
            self.dict_info["Force (pN)"]= self.force
            self.dict_info["index start approach"]= self.index_start_approach
            self.dict_info["index end approach"]= self.index_end_approach
            self.dict_info["index start retract"]= self.index_start_retract
            self.dict_info["index end retract"]= self.index_end_retract
            self.dict_info['time (ms)'] =self.time
            self.K= self.parameters['spring constant']
            self.sensitivity= self.parameters['sensitivity']
        # plot according to what the user selected on x-axis and y-axis
        #self.get_combo_values()
        
        
    def noise_level(self):
        """
        Noise level via RMS estimation

        Returns
        -------
        None.

        """
        percentage= self.percentage_noise.get()
        self.RMS= NL.get_RMS(self.dict_info["Force (pN)"][ self.dict_info["index start retract"]: self.dict_info["index end retract"]], percentage)  
        self.label_noise.config(text= "RMS:     "+str(self.RMS)+ "  pN"+ "      Set %  here: ")
        
        
    def get_deflection_vs_piezo(self):
        """
        This function extracts the deflection vs piezo , time from both JPK and tdms
        by calling parse_tdms (if it is tdms file) function or parse_jpk (if is JPK file)

        Returns
        -------
        None.

        """

        if self.combo_file.get() == ".tdms":
            self.channel_data_deflection, self.channel_data_piezo, self.time= tdms.parse_tdms(self.path)

            self.dict_info['time (ms)'] =self.time
            self.dict_info['Piezo (V)'] =self.channel_data_piezo
            self.dict_info['Deflection (V)'] =self.channel_data_deflection
            self.dict_raw['Piezo (V)'] =self.channel_data_piezo
            self.dict_raw['Deflection (V)'] =self.channel_data_deflection
            

        elif self.combo_file.get() == ".jpk":
            self.force, self.time, self.distance, self.extension, self.parameters, self.index_start_approach, self.index_end_approach, self.index_start_retract, self.index_end_retract= JPK.parse_jpk(self.path)


    def get_combo_values(self):
        """
        Gets the combobox values selected by the user and plots them

        Returns
        -------
        None.

        """

        figure = Figure(figsize=(7,5), dpi=100)
        ax=figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(figure, master=self.subframe_plots)
        canvas.draw()
        widget= canvas.get_tk_widget()

        widget.grid(row=8, column=1)
        # navigation toolbar
        toolbarFrame = Frame(master= self.subframe_plots)
        toolbarFrame.grid(row=11,column=1)

        toolbar_main = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame)
        ax.grid()
        
        xaxis=  str(self.combo_xaxis.get())
        yaxis= str(self.combo_yaxis.get())
        #if xaxis == "time (ms)":
         #   ax.set_xlim([min(self.dict_info[xaxis][self.dict_info['index start retract']: self.dict_info['index end retract']]- 50), 
          #              max(self.dict_info[xaxis][self.dict_info['index start retract']: self.dict_info['index end retract']])])
        
        # if the user wants to plot approach and retract seperatly
        if self.approachOn.get()==1 and self.retractOn.get()==1:
            
            ax.plot(self.dict_info[xaxis][self.dict_info['index start retract']: self.dict_info['index end retract']], 
                    self.dict_info[yaxis][self.dict_info['index start retract']: self.dict_info['index end retract']])

            ax.plot(self.dict_info[xaxis][self.dict_info['index start approach']: self.dict_info['index end approach']], 
                    self.dict_info[yaxis][self.dict_info['index start approach']: self.dict_info['index end approach']])
            ax.set_xlabel(xaxis)
            ax.set_ylabel(yaxis)
        # if the user wants to plot approach and retract together
        else:
            ax.plot(self.dict_info[xaxis], self.dict_info[yaxis])
            
            ax.set_xlabel(xaxis)
            ax.set_ylabel(yaxis)


    def apply_filter(self):
        """
        Applies and plots the filter

        Returns
        -------
        None.

        """
        start= time.time()
        # get the value from combobox
        # permet de piocher plus facilement dans le dico d'informations
        xaxis=  str(self.combo_xaxis.get())
        yaxis= str(self.combo_yaxis.get())
        # if the user gives a value for the smoothing window
        smoothing_window= self.smoothing_window.get()

        if smoothing_window and smoothing_window> 3:
            # applying savitzgy-golay filter
            self.dict_info['Force (pN)'][self.dict_info['index start retract']: self.dict_info['index end retract']]= tdms.ApplySavgol(self.dict_info[yaxis][self.dict_info['index start retract']: self.dict_info['index end retract']], smoothing_window )
            #plotting
            figure = Figure(figsize=(7,5), dpi=100)
            ax=figure.add_subplot(111)
            canvas = FigureCanvasTkAgg(figure, master=self.subframe_plots)
            canvas.draw()
            self.widget_main= canvas.get_tk_widget()
            self.widget_main.grid(row=8, column=1)
            # navigation toolbar
            toolbarFrame = Frame(master= self.subframe_plots)
            toolbarFrame.grid(row=11,column=1)
            self.toolbar_main = NavigationToolbar2Tk(canvas, 
                                                     toolbarFrame) 
            self.toolbar_main.update()
            ax.grid()
            ax.plot(self.dict_info[xaxis][self.dict_info['index start retract']: self.dict_info['index end retract']], 
                    self.dict_info['Force (pN)'][self.dict_info['index start retract']: self.dict_info['index end retract']])
            ax.plot(self.dict_info[xaxis][self.dict_info['index start approach']: self.dict_info['index end approach']], 
                    self.dict_info[yaxis][self.dict_info['index start approach']: self.dict_info['index end approach']])

            ax.set_xlabel(xaxis)
            ax.set_ylabel(yaxis)
        else:
            tkinter.messagebox.showinfo('warning' ,'The smoothing value has to be an odd number bigger than 3')

        end= time.time()
        print('smoothing ', round(end-start, 2))
    
        
    def change_parameters(self):
        """
        This function is called when the user changes
        one of the parameters (K (cste spring), sensitivity, invOLS)
        it plots the new curve after the parameters are changed by the user 

        Returns
        -------
        None.

        """
        #plot
        figure = Figure(figsize=(7,5), dpi=100)
        ax=figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(figure, master=self.subframe_plots)
        canvas.draw()
        self.widget_main= canvas.get_tk_widget()
        self.widget_main.grid(row=8, column=1) 
        toolbarFrame = Frame(master= self.subframe_plots)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_main = NavigationToolbar2Tk(canvas, 
                                                     toolbarFrame) 
        self.toolbar_main.update()
        ax.grid()

        # if the user gives a new invols value
        if self.entry_invols.get():
            self.invOLS= self.entry_invols.get()
            # calculate new disrance and force
            self.dict_info["Distance (nm)"],  self.dict_info["Force (pN)"]= tdms.FDmodifParms(self.channel_data_deflection, self.channel_data_piezo,  self.K, self.invOLS, self.piezo_gain, self.sensitivity)
            self.dict_info["Indentation/Separation (nm)"]= tdms.ComputeExtension(self.dict_info["Force (pN)"], self.dict_info["Distance (nm)"], float(self.K))
            self.dict_info["Indentation/Separation (nm)"]=self.dict_info["Indentation/Separation (nm)"]+max(self.dict_info["Indentation/Separation (nm)"])
        
        # if the user give a new spring constant 
        if self.entry_k.get():
            self.k= self.entry_k.get()
            #calculate the new values
            self.dict_info["Distance (nm)"],  self.dict_info["Force (pN)"]= tdms.FDmodifParms(self.channel_data_deflection, self.channel_data_piezo,  self.k, self.invOLS, self.piezo_gain, self.sensitivity)
            self.dict_info["Indentation/Separation (nm)"]= tdms.ComputeExtension(self.dict_info["Force (pN)"], self.dict_info["Distance (nm)"], float(self.k))
            self.dict_info["Indentation/Separation (nm)"]= self.dict_info["Indentation/Separation (nm)"]+max(self.dict_info["Indentation/Separation (nm)"])
        
       # self.correct_point_contact()
        #self.apply_corrections()


    def correct_virtual_deflection(self):
        """
        Correct the virtual deflection once the user press the button
        the corrected deflection is plotted on the GUI

        Returns
        -------
        None.

        """
        #figure widgets
        figure = Figure(figsize=(7,5), dpi=100)
        ax=figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(figure, master=self.subframe_plots)
        canvas.draw()
        self.widget_main= canvas.get_tk_widget()
        self.widget_main.grid(row=8, column=1) 
        toolbarFrame = Frame(master= self.subframe_plots)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_main = NavigationToolbar2Tk(canvas, 
                                                     toolbarFrame) 
        self.toolbar_main.update()
        ax.grid()
        xaxis=  str(self.combo_xaxis.get())
        yaxis= str(self.combo_yaxis.get())
        # if a polynomial order is set 
        Npoly= self.Npoly.get()
        hysteresis= self.hysteresis.get()
        #if it is tdms file
        if self.combo_file.get() == ".tdms":
            # if the user wants to correct only from approach
            if self.FromApp.get() == 1:
                # if the user gives the % of approach
                if self.pourcentage_approach.get():
                    # get the % of the approach
                    percentage= self.pourcentage_approach.get()
                                        #plot
                    if self.change_invOLS.get()==1 or self.change_K.get()==1:
                        self.change_parameters()
                        self.dict_info["Force (pN)"]= tdms.CorrectVirtualDeflection( self.dict_info["Force (pN)"], self.dict_info["Distance (nm)"],  Npoly,
                                                                                self.dict_info['index start approach'], self.dict_info['index end approach'], 
                                                                                self.dict_info['index start retract'], self.dict_info['index end retract'],
                                                                                percentage,  hysteresis=hysteresis)

                    else:                                         
                        # correct the virtual deflection
                        corrected_deflection= tdms.CorrectVirtualDeflection( self.channel_data_deflection,
                                    self.distance, Npoly, self.index_start_approach, self.index_end_approach, self.index_start_retract,
                                    self.index_end_retract, percentage, hysteresis=hysteresis )
                        # get the new force after the deflection is corrected
                        distance, force = tdms.FDmodifParms(corrected_deflection, self.channel_data_piezo, self.K, self.invOLS, self.piezo_gain, self.sensitivity)
                        self.dict_info['Deflection (nm)']= tdms.DeflectionInNanometer(np.asarray(corrected_deflection, dtype='float64'), float(self.invOLS))
                        # store info in dico
                        self.dict_info['Force (pN)']= force
                        self.dict_info['Deflection (V)']= corrected_deflection
                        # on recupere les vrais valeur pour appliquer les prochaines corrections dessus, pour eviter d'appliquer une correction sur la correction 
                        self.dict_info['Deflection (V)']= tdms.CorrectVirtualDeflection( self.channel_data_deflection,
                                    self.distance, Npoly, self.index_start_approach, self.index_end_approach, self.index_start_retract,
                                    self.index_end_retract, percentage, hysteresis=-hysteresis )
                        

            # if the user wants to correct the deflection from both retract and approach               
            elif self.FromApp.get() == 0 and  self.pourcentage_retract.get():
                # get the % of retract
                percentage_retract= self.pourcentage_retract.get()
                # get the % of approach
                percentage_approach= self.pourcentage_approach.get()
                
                if self.change_invOLS.get()==1 or self.change_K.get()==1:
                    
                    self.change_parameters()
                    self.dict_info["Force (pN)"]= tdms.CorrectDeflectionFromRetract( self.dict_info["Force (pN)"], self.dict_info["Distance (nm)"],  Npoly,self.dict_info['index start approach'], 
                                                                                    self.dict_info['index end approach'], self.dict_info['index start retract'], self.dict_info['index end retract'], percentage_retract, percentage_approach)
                else:

                    # get corrected deflection
                    corrected_deflection= tdms.CorrectDeflectionFromRetract(self.channel_data_deflection, self.distance,  Npoly, self.index_start_approach, 
                                                                            self.index_end_approach, self.index_start_retract, self.index_end_retract, percentage_retract, percentage_approach)
                    # get the new values of force after the deflection has been corrected 
                    distance, force = tdms.FDmodifParms(corrected_deflection, self.channel_data_piezo, self.K, self.invOLS, self.piezo_gain, self.sensitivity)
                    #store the information in dico
                    self.dict_info['Force (pN)']= force
                    self.dict_info['Deflection (V)']= corrected_deflection



        #same for jpk
        elif self.combo_file.get() == ".jpk":
            if self.FromApp.get() == 1:
                if self.pourcentage_approach.get():
                    percentage= self.pourcentage_approach.get()
                    self.corrected_force= tdms.CorrectVirtualDeflection( self.force,
                                    self.distance, Npoly, self.index_start_approach, self.index_end_approach, self.index_start_retract,
                                    self.index_end_retract, percentage )
                    self.dict_info['Force (pN)']= self.corrected_force

                    
            elif self.FromApp.get() == 0 and  self.pourcentage_retract.get():
                percentage_retract= self.pourcentage_retract.get()
                percentage_approach= self.pourcentage_approach.get()
                self.corrected_force= tdms.CorrectDeflectionFromRetract(self.force, self.distance,  Npoly, self.index_start_approach, 
                            self.index_end_approach, self.index_start_retract, self.index_end_retract, percentage_retract, percentage_approach)
                
                self.dict_info['Force (pN)']= self.corrected_force
        self.get_combo_values()
        self.apply_corrections()
        
        if self.SmoothingOn.get()==1:
            self.apply_filter()
        
        if self.DetectPeaksOn.get() ==1:
            self.plot_detected_peaks()
        if self.combo_fit_model.get() == "fit WLC":
        #if self.WLCOn.get() == 1:
            self.plot_WLC_model()
            
        if self.combo_fit_model.get()  == "fit FJC":
            self.plot_FJC_model()

        
                
    def apply_virtual_deflection_correction(self):
        """
        Applies the corrections if the needed buttons are checked

        Returns
        -------
        None.

        """
        figure = Figure(figsize=(7,5), dpi=100)
        ax=figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(figure, master=self.subframe_plots)
        canvas.draw()
        self.widget_main= canvas.get_tk_widget()
        self.widget_main.grid(row=8, column=1) 
        toolbarFrame = Frame(master= self.subframe_plots)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_main = NavigationToolbar2Tk(canvas, 
                                                     toolbarFrame) 
        self.toolbar_main.update()
        ax.grid()
        if self.CorrectVirtualDeflection.get()==1:
            self.correct_virtual_deflection()
            
        elif self.CorrectVirtualDeflection.get()==0:

            xaxis=  str(self.combo_xaxis.get())
            yaxis= str(self.combo_yaxis.get())
        # if the user wants to plot approach and retract seperatly
            if self.approachOn.get()==1 and self.retractOn.get()==1:
            
                ax.plot(self.dict_raw[xaxis][self.dict_info['index start retract']: self.dict_info['index end retract']], 
                        self.dict_raw[yaxis][self.dict_info['index start retract']: self.dict_info['index end retract']])

                ax.plot(self.dict_raw[xaxis][self.dict_info['index start approach']: self.dict_info['index end approach']], 
                        self.dict_raw[yaxis][self.dict_info['index start approach']: self.dict_info['index end approach']])
                ax.set_xlabel(xaxis)
                ax.set_ylabel(yaxis)
                # if the user wants to plot approach and retract together
            else:
                ax.plot(self.dict_raw[xaxis], self.dict_raw[yaxis])
                ax.set_xlabel(xaxis)
                ax.set_ylabel(yaxis)
        
        
    def show_point_contact_retract(self):
        """
        Shows the point of contact of the retract curve once the button is pressed 

        Returns
        -------
        None.

        """
        #prepare figure widgets
        figure = Figure(figsize=(7,5), dpi=100)
        ax=figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(figure, master=self.subframe_plots)
        canvas.draw()
       
        self.widget_main= canvas.get_tk_widget()
        canvas.get_tk_widget()
        self.widget_main.grid(row=8, column=1) 
        toolbarFrame = Frame(master= self.subframe_plots)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_main = NavigationToolbar2Tk(canvas, 
                                                     toolbarFrame) 
        self.toolbar_main.update()
        ax.grid()
        xaxis=  str(self.combo_xaxis.get())
        yaxis= str(self.combo_yaxis.get())
        if self.CpRetractOn.get()==1:
        # get the intersection point with 0
            self.intersection_retract= cp.FindContactPoint(self.dict_info[xaxis][self.dict_info['index start retract']: self.dict_info['index end retract']]
                                            , self.dict_info[yaxis][self.dict_info['index start retract']: self.dict_info['index end retract']])
        # plot curves and intersection point
            # ax.plot(self.dict_info[xaxis][self.dict_info['index start retract']: self.dict_info['index end retract']], 
            #             self.dict_info[yaxis][self.dict_info['index start retract']: self.dict_info['index end retract']])
        
                
            # ax.plot(self.dict_info[xaxis][self.dict_info['index start approach']: self.dict_info['index end approach']], 
            #                 self.dict_info[yaxis][self.dict_info['index start approach']: self.dict_info['index end approach']])
        
            #ax.plot(*self.intersection_retract, 'ro', alpha=0.7, ms=10)
        elif self.CpRetractOn.get()==0:
            self.get_combo_values()
            # ax.plot(self.dict_info[xaxis][self.dict_info['index start retract']: self.dict_info['index end retract']], 
            #             self.dict_info[yaxis][self.dict_info['index start retract']: self.dict_info['index end retract']])
        
                
            # ax.plot(self.dict_info[xaxis][self.dict_info['index start approach']: self.dict_info['index end approach']], 
            #                 self.dict_info[yaxis][self.dict_info['index start approach']: self.dict_info['index end approach']])
        
        if self.intersection_retract ==None:
            #tkinter.messagebox.showinfo('warning' ,'No intersection point found')

            self.nextFile()
            
        ax.set_xlabel(str(xaxis))
        ax.set_ylabel(str(yaxis))

    def show_point_contact_approach(self):
        """
        Shows the point of contact of the approach curve once the button is pressed 

        Returns
        -------
        None.

        """
        #prepare widgets of the figure
        figure = Figure(figsize=(7,5), dpi=100)
        ax=figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(figure, master=self.subframe_plots)
        canvas.draw()
        self.widget_main= canvas.get_tk_widget()
        self.widget_main.grid(row=8, column=1) 
        toolbarFrame = Frame(master= self.subframe_plots)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_main = NavigationToolbar2Tk(canvas, 
                                                     toolbarFrame) 
        self.toolbar_main.update()
        ax.grid()
        xaxis=  str(self.combo_xaxis.get())
        yaxis= str(self.combo_yaxis.get())
        
        zero_axis= np.zeros(len(self.dict_info["Deflection (V)"]))
        if self.CpApproachOn.get()==1:
        #get contact point approach
            self.intersection_approach= cp.FindContactPoint(self.dict_info[xaxis][self.dict_info['index start approach']: self.dict_info['index end approach']][::-1]
                                                            , self.dict_info[yaxis][self.dict_info['index start approach']: self.dict_info['index end approach']][::-1])
                                                          
        
        #plot
            # ax.plot(self.dict_info[xaxis][self.dict_info['index start retract']: self.dict_info['index end retract']], 
            #                 self.dict_info[yaxis][self.dict_info['index start retract']: self.dict_info['index end retract']])
        
            # ax.plot(self.dict_info[xaxis][self.dict_info['index start approach']: self.dict_info['index end approach']], 
            #                     self.dict_info[yaxis][self.dict_info['index start approach']: self.dict_info['index end approach']])

            #ax.plot(*self.intersection_approach, 'ro', alpha=0.7, ms=10)
        elif self.CpApproachOn.get()==0:
            self.get_combo_values()
            # ax.plot(self.dict_info[xaxis][self.dict_info['index start retract']: self.dict_info['index end retract']], 
            #                 self.dict_info[yaxis][self.dict_info['index start retract']: self.dict_info['index end retract']])
        
            # ax.plot(self.dict_info[xaxis][self.dict_info['index start approach']: self.dict_info['index end approach']], 
            #                     self.dict_info[yaxis][self.dict_info['index start approach']: self.dict_info['index end approach']])

        if self.intersection_approach==None:
            self.nextFile()
        
            
        ax.set_xlabel(str(xaxis))
        ax.set_ylabel(str(yaxis))
    
    def correct_point_contact(self):
        """This function corrects the CP
            ==> offset to 0 on the x-axis
        """

        xaxis=  str(self.combo_xaxis.get())
        yaxis= str(self.combo_yaxis.get())
        
        if self.intersection_approach ==None or self.intersection_retract==None:

            tkinter.messagebox.showinfo('warning' ,'please, start by detecting the contact points of approach and retract')

        else:
            if self.CorrectCP.get()==1:
                self.dict_info[xaxis][self.dict_info['index start retract']: self.dict_info['index end retract']]= cp.CorrectContactPoint(self.dict_info[xaxis][self.dict_info['index start retract']: self.dict_info['index end retract']], self.intersection_retract[0])
                if xaxis != "time (ms)":
                    self.dict_info[xaxis][self.dict_info['index start approach']: self.dict_info['index end approach']]= cp.CorrectContactPoint(self.dict_info[xaxis][self.dict_info['index start approach']: self.dict_info['index end approach']], self.intersection_approach[0])
                self.get_combo_values()

            elif self.CorrectCP.get()==0:
                self.get_combo_values()

        
    def apply_corrections(self):
        self.show_point_contact_approach()
        self.show_point_contact_retract()
        self.correct_point_contact()

        
        
    def plot_detected_peaks(self):
        """
        plots the peaks

        Returns
        -------
        None.

        """
        #figure widgets
        figure = Figure(figsize=(7,5), dpi=100)
        ax=figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(figure, master=self.subframe_plots)
        canvas.draw()
        self.widget_main= canvas.get_tk_widget()
        self.widget_main.grid(row=8, column=1)
        # navigation toolbar
        toolbarFrame = Frame(master= self.subframe_plots)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_main = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
        self.toolbar_main.update()
        ax.grid()
        ax.set_xlabel("Indentation/Separation (nm)")
        ax.set_ylabel("Force (pN)")
        # get the value of the width set by the user
        w =self.width.get()
        p= self.prominence.get()
        distance= self.range_before_peak.get()

        #renisialize
        try:
            self.dict_info['peaks']
            del self.dict_info['peaks']
        except:
            None


        # try:
        #     self.dict_info['smoothed']
        # except:
        #     self.dict_info['smoothed']=self.dict_info['Force (pN)'][self.dict_info['index start retract']: self.dict_info['index end retract']]
            
        # find the peaks
        self.peaks, self.properties= func.find_peaks(self.dict_info['Force (pN)'][self.dict_info['index start retract']: self.dict_info['index end retract']], width=w, prominence=p)
        # plot
        ax.plot(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']], 
                    self.dict_info['Force (pN)'][self.dict_info['index start retract']: self.dict_info['index end retract']])
        
        ax.plot(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start approach']: self.dict_info['index end approach']], 
                    self.dict_info['Force (pN)'][self.dict_info['index start approach']: self.dict_info['index end approach']])
        # if a number of peaks is NOT set
        if not self.N_peaks.get():
            self.dict_info['peaks']= self.peaks

            for i in self.dict_info['peaks']:
                ax.plot(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][i],
                        self.dict_info['Force (pN)'][self.dict_info['index start retract']: self.dict_info['index end retract']][i],'or')
        
        # if a number of peaks is set: the program takes the higher peaks according to the number set by the user 

        elif self.N_peaks.get():
            N_peaks= self.N_peaks.get()
            list_all_heights=[]
            #get all the heights of the peaks
            for i in range(len(self.properties)):
                for j in range(len(self.properties[i]['peak_heights'])):
                    list_all_heights.append(self.properties[i]['peak_heights'][j])
            #sort the heights
            list_all_heights=sorted(list_all_heights) 
            # keep only last highest peaks according tot the user's number N_peaks
            list_all_heights= list_all_heights[-N_peaks:]
            # get the original index of each peak                
            theset = frozenset(list_all_heights)
            theset = sorted(theset, reverse=True)
            thedict = {}
            n_peaks=[]
            if len(list_all_heights) < N_peaks:
                N_peaks=len(list_all_heights)
                tkinter.messagebox.showinfo('information' ,' Only '+ str(N_peaks) + ' peaks were detected')

            for j in range(N_peaks):
                positions = [i for i, x in enumerate(list_all_heights) if x == theset[j]]
                thedict[theset[j]] = positions
                for i in range(len(self.properties)):
                    #append the heighest peaks in a list
                    n_peaks.append(self.peaks[list(self.properties[i]['peak_heights']).index(theset[j])])
            self.dict_info['peaks']= sorted(n_peaks)
            #plot all peaks
            self.lc_entry.delete('0', 'end')
            for i in self.dict_info['peaks']:
                self.lc_entry.insert(0, str(int(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][i]+0.1))+ ', ')
                
                
                ax.plot(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][i], 
                        self.dict_info['Force (pN)'][self.dict_info['index start retract']: self.dict_info['index end retract']][i], 'or')
        
        
        self.tab_peak_detection.delete(*self.tab_peak_detection.get_children())

        d, self.slope,intercept= func.get_slopes(self.dict_raw["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']], 
                                            self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']], self.dict_info['peaks'], distance,
                                            self.dict_info["time (ms)"][self.dict_info['index start retract']: self.dict_info['index end retract']])
        
        
        for i in range(len(self.dict_info['peaks'])): 
            self.tab_peak_detection.insert('' ,i ,values=(i+1, "{:.2e}".format(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][self.dict_info['peaks'][i]]), 
                                                          "{:.2e}".format(self.dict_info['Force (pN)'][self.dict_info['index start retract']: self.dict_info['index end retract']][self.dict_info['peaks'][i]] ), "{:.2e}".format(self.slope[i]*10**3) ))
            self.tab_peak_detection.grid(row=2, column=0, sticky=W+E+S+N)   
            self.scrollbar.grid(row=2, column=1, sticky="ns")
            
        self.delete_button.grid(row=0, column= 0)

        print('Total time (ms) ', self.dict_info['time (ms)'][-1])
        print('Total extension (nm)', self.dict_raw["Indentation/Separation (nm)"][-1])
        print('Velocity (nm/s) ', self.dict_raw["Indentation/Separation (nm)"][-1]/(self.dict_info['time (ms)'][-1]*10**-3))
        self.dict_info['Velocity (nm/s)']=self.dict_raw["Indentation/Separation (nm)"][-1]/(self.dict_info['time (ms)'][-1]*10**-3)
        
        if len(self.dict_info['peaks']) ==  0 and self.JumpOn.get()==1: 
            self.nextFile()
            
    
    
    def plot_WLC_model(self):
        """
        This function fits the WLC model

        Returns
        -------
        None.

        """
        

        figure = Figure(figsize=(7,5), dpi=100)
        ax=figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(figure, master=self.subframe_plots)
        canvas.draw()
        self.widget_main= canvas.get_tk_widget()
        self.widget_main.grid(row=8, column=1)
        # navigation toolbar
        toolbarFrame = Frame(master= self.subframe_plots)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_main = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
        self.toolbar_main.update()
        ax.grid()
        ax.set_xlabel("Indentation/Separation (nm)")
        ax.set_ylabel("Force (pN)")

        if self.range_before_peak.get()== 0 or len(self.contour_length.get())== 0:
            tkinter.messagebox.showinfo('warning' ,'please, set range before peak and/or contour length')
            
        else:
            
            Lc= self.contour_length.get()
            Lc= Lc[:-2]
            range_before_peak= self.range_before_peak.get()
  

            ax.set_ylim([int(min(self.dict_info["Force (pN)"])), int(max(self.dict_info["Force (pN)"])+100)])
            ax.plot(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']], 
                self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']])
        
            ax.plot(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start approach']: self.dict_info['index end approach']], 
                self.dict_info["Force (pN)"][self.dict_info['index start approach']: self.dict_info['index end approach']])

            self.ListLc= []
            d={ 'dis1':[], 'disTMP':[], 'dis2':[], 'index_dis2':[]}
            lc= Lc.split(',')
            lc = [ int(x) for x in lc ]
            self.dict_info['peaks']= sorted(self.dict_info['peaks'])

            for i in range(len( self.dict_info['peaks'])):
                # print('extension at peak ', extension[index_start_retract: index_end_retract][i])
                d['dis1'].append(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][self.dict_info['peaks'][i]])
                #compute selected value = displacement[peak_index]-distance given by the user in nm
            for i in range(len( self.dict_info['peaks'])):
                d['disTMP'].append(d['dis1'][i]- range_before_peak)

                #search for closest value to selected one (selected one is distance before peak)
                d['dis2'].append(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']].flat[np.abs(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']] - d['disTMP'][i]).argmin()])
                #put found values in dictionnary of info
                d['index_dis2']= int(list(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']]).index(d['dis2'][i]))
                start= d['index_dis2']
                
               # print('fitting start at ', self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][start])
                end= self.dict_info['peaks'][i]
                popt, pcov = curve_fit(wlc.WLC, self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][start: end] ,
                                   self.dict_info['Force (pN)'][self.dict_info['index start retract']: self.dict_info['index end retract']][start: end],
                       p0=[self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][self.dict_info['peaks'][i]]+5],

                       bounds=(( self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][self.dict_info['peaks'][i]]),
                               (self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][self.dict_info['peaks'][i]]+10)),
                       method='trf') #Algorithm to perform minimization.‘trf’ : Trust Region Reflective algorithm, particularly suitable for large sparse problems with bounds. Generally robust method.
                self.ListLc.extend(popt)

                
                print('starting  Lc', lc[i],  ' (nm) optimal Lc : ', popt, ' nm')

                y= wlc.WLC(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][0:int(len(self.force)/2)], *popt)
                # intersection of the WLC fitting with the x-axis constant max Force value
                intersection= func.find_intersection(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']] 
                                                     ,y)
                if intersection==None:
                    tkinter.messagebox.showinfo('warning' ,'Set a correct contour length value')

                # get the closest value to the intersection point from the extension data to be able to get index later
                close_value_intersection= self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']].flat[np.abs(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']] - intersection[0]).argmin()]
                # Get the index of the intersection
                index_intersection= list(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']]).index(close_value_intersection)

                
                ax.plot( self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][0: index_intersection],
                        wlc.WLC(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][0: index_intersection], *popt))
                
                for i in self.dict_info['peaks']: 
                    ax.plot(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][i], 
                        self.dict_info['Force (pN)'][self.dict_info['index start retract']: self.dict_info['index end retract']][i], 'or')  
                    
                    
    def plot_FJC_model(self):
        """
        This function fits the WLC model

        Returns
        -------
        None.

        """
        

        figure = Figure(figsize=(7,5), dpi=100)
        ax=figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(figure, master=self.subframe_plots)
        canvas.draw()
        self.widget_main= canvas.get_tk_widget()
        self.widget_main.grid(row=8, column=1)
        # navigation toolbar
        toolbarFrame = Frame(master= self.subframe_plots)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_main = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
        self.toolbar_main.update()
        ax.grid()
        ax.set_xlabel("Indentation/Separation (nm)")
        ax.set_ylabel("Force (pN)")

        if self.range_before_peak.get()== 0 or len(self.contour_length.get())== 0:
            tkinter.messagebox.showinfo('warning' ,'please, set range before peak and/or contour length')
            
        else:
            
            Lc= self.contour_length.get()
            Lc= Lc[:-2]
            range_before_peak= self.range_before_peak.get()
  

            ax.set_ylim([int(min(self.dict_info["Force (pN)"])), int(max(self.dict_info["Force (pN)"])+100)])
            ax.plot(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']], 
                self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']])
        
            ax.plot(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start approach']: self.dict_info['index end approach']], 
                self.dict_info["Force (pN)"][self.dict_info['index start approach']: self.dict_info['index end approach']])

            self.ListLcFJC= []
            d={ 'dis1':[], 'disTMP':[], 'dis2':[], 'index_dis2':[]}
            lc= Lc.split(',')
            lc = [ int(x) for x in lc ]
            self.dict_info['peaks']= sorted(self.dict_info['peaks'])

            for i in range(len( self.dict_info['peaks'])):
                # print('extension at peak ', extension[index_start_retract: index_end_retract][i])
                d['dis1'].append(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][self.dict_info['peaks'][i]])
                #compute selected value = displacement[peak_index]-distance given by the user in nm
            for i in range(len( self.dict_info['peaks'])):
                d['disTMP'].append(d['dis1'][i]- range_before_peak)

                #search for closest value to selected one (selected one is distance before peak)
                d['dis2'].append(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']].flat[np.abs(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']] - d['disTMP'][i]).argmin()])
                #put found values in dictionnary of info
                d['index_dis2']= int(list(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']]).index(d['dis2'][i]))
                start= d['index_dis2']
                
               # print('fitting start at ', self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][start])
                end= self.dict_info['peaks'][i]
                popt, pcov = curve_fit(wlc.WLC, self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][start: end] , 
                                   self.dict_info['Force (pN)'][self.dict_info['index start retract']: self.dict_info['index end retract']][start: end],
                       p0=[lc[i]], 
                       bounds=((lc[i]), (start*10)),
                       method='trf') #Algorithm to perform minimization.‘trf’ : Trust Region Reflective algorithm, particularly suitable for large sparse problems with bounds. Generally robust method.
                self.ListLcFJC.extend(popt)

                
                print('starting  Lc', lc[i],  ' (nm) optimal Lc : ', popt, ' nm')

                y= fjc.FJC(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][0:int(len(self.force)/2)], *popt)
                # intersection of the WLC fitting with the x-axis constant max Force value
                #intersection= func.find_intersection(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']] 
                 #                                    ,y)
                #if intersection==None:
                 #   tkinter.messagebox.showinfo('warning' ,'Set a correct contour length value')

                # get the closest value to the intersection point from the extension data to be able to get index later
                #close_value_intersection= self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']].flat[np.abs(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']] - intersection[0]).argmin()]
                # Get the index of the intersection
                #index_intersection= list(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']]).index(close_value_intersection)

                
                ax.plot( self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][start: end ],
                        fjc.FJC(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][start : end], *popt))
                
                for i in self.dict_info['peaks']: 
                    ax.plot(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][i], 
                        self.dict_info['Force (pN)'][self.dict_info['index start retract']: self.dict_info['index end retract']][i], 'or')
 
    def plot_FcLc(self):
        """
        This functions shows the Force vs Contour length
        Fc: force in pN
        Lc: contour length i nm 

        Returns
        -------
        None.

        """

        figure = Figure(figsize=(7,5), dpi=100)
        ax=figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(figure, master=self.subframe_plots)
        canvas.draw()
        self.widget_main= canvas.get_tk_widget()
        self.widget_main.grid(row=8, column=1)
        # navigation toolbar
        toolbarFrame = Frame(master= self.subframe_plots)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_main = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
        self.toolbar_main.update()
        ax.grid()
        ax.set_xlabel("Contour Length (nm)")
        ax.set_ylabel("Force (pN)")
        self.dict_info['Contour Length (nm)'], self.dict_info['Fc (pN)']= Lc.FindLc(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']] , self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']])

        w= self.width.get()
        p= self.prominence.get()
        number_wanted_peaks= self.N_peaks.get()
        d= { 'Fc': self.dict_info['Fc (pN)'], 'Lc':self.dict_info['Contour Length (nm)']}
        df = pd.DataFrame(data=d)
        df = df.dropna()
        if self.threshold.get() :
            df['Fc']= df[df['Fc'] > self.threshold.get()]

        self.dict_info['Contour Length (nm)']=np.array(df['Lc'])
        self.dict_info['Fc (pN)']= np.array(df['Fc'])
        peaks, properties= find_peaks(self.dict_info['Fc (pN)'], width=w, prominence=p, height=0)
        n_peaks= Lc.return_highest_peaks(properties, peaks, number_wanted_peaks )
        for i in n_peaks:
            ax.plot(self.dict_info['Contour Length (nm)'][i],self.dict_info['Fc (pN)'][i], 'or' )
        self.dict_info["Force (pN)"]= np.array(self.dict_info["Force (pN)"])
        closest=[]
        self.idx_peaks_2nd_detection=[]
        for i in n_peaks:
            #find closest value to the max force in Lc vs force profile in force dataset
            closest.append(self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']].flat[np.abs(self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']] - self.dict_info['Fc (pN)'][i]).argmin()])

        for i in range(len(closest)):
            self.idx_peaks_2nd_detection.append(int(list(self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']]).index(closest[i])))
        ax.scatter(self.dict_info['Contour Length (nm)'], self.dict_info['Fc (pN)'])
    
    def plot_loading_rate(self):
        """
        This function plots the loading rates

        Returns
        -------
        None.

        """

        figure = Figure(figsize=(7,5), dpi=100)
        ax=figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(figure, master=self.subframe_plots)
        canvas.draw()
        self.widget_main= canvas.get_tk_widget()
        self.widget_main.grid(row=8, column=1)
        # navigation toolbar
        toolbarFrame = Frame(master= self.subframe_plots)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_main = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
        self.toolbar_main.update()
        ax.grid()
        distance= self.range_before_peak.get()
        
        self.intersection_time= cp.FindContactPoint(self.dict_info['time (ms)'], self.dict_info['Deflection (V)'])
        self.dict_info['time (ms)']= cp.CorrectContactPoint(self.dict_info['time (ms)'], self.intersection_time[0])
        
        d, self.slope,intercept= func.get_slopes(self.dict_raw["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']], 
                                            self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']], self.dict_info['peaks'], distance,
                                            self.dict_info["time (ms)"][self.dict_info['index start retract']: self.dict_info['index end retract']])
        
        ax.plot(self.dict_info["time (ms)"][self.dict_info['index start retract']: self.dict_info['index end retract']], self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']])
        ax.plot(self.dict_info["time (ms)"][self.dict_info['index start approach']: self.dict_info['index end approach']], self.dict_info["Force (pN)"][self.dict_info['index start approach']: self.dict_info['index end approach']])



        for i in range(len(d['peaks'])):
            ax.plot(self.dict_info["time (ms)"][self.dict_info['index start retract']: self.dict_info['index end retract']][d['index_dis2'][i]:d['peaks'][i]], 
                    self.slope[i]*np.array(self.dict_info["time (ms)"][self.dict_info['index start retract']: self.dict_info['index end retract']][d['index_dis2'][i]:d['peaks'][i]])+intercept[i],'r')
            print('Peak '+str(i+1)+ ' : '+'Loading Rate '+ str(self.slope[i]*10**3), ' (pN/s)')

        self.apply_corrections()
        ax.set_xlabel("time (ms)")
        ax.set_ylabel("Force (pN)")

    def plot_peak_FcLc(self):
        """
        plots the force vs contour length profile

        Returns
        -------
        None.

        """
        figure = Figure(figsize=(7,5), dpi=100)

        ax=figure.add_subplot(111)

        canvas = FigureCanvasTkAgg(figure, master=self.subframe_plots)
        canvas.draw()

        self.widget_main= canvas.get_tk_widget()
        self.widget_main.grid(row=8, column=1)

        
        # navigation toolbar
        toolbarFrame = Frame(master= self.subframe_plots)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_main = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
        self.toolbar_main.update()


        ax.grid()

        try:
            self.dict_info['Contour Length (nm)']
            self.dict_info['Fc (pN)']
        except:
            tkinter.messagebox.showinfo('warning' ,'please, Click plot Fc vs Lc first') 
            
        
        ax.plot(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']], 
                 self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']])
        ax.plot(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start approach']: self.dict_info['index end approach']], 
                 self.dict_info["Force (pN)"][self.dict_info['index start approach']: self.dict_info['index end approach']])

        #plot it
        for i in self.idx_peaks_2nd_detection:
            ax.plot(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][i],
                self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']][i],
                'or')

        for i in range(len(self.idx_peaks_2nd_detection)): 
           
            if self.idx_peaks_2nd_detection[i] not in self.dict_info['peaks']:
                
                self.tab_peak_detection.insert('' ,i ,values=(i+1, "{:.2e}".format(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][self.idx_peaks_2nd_detection[i]]), "{:.2e}".format(self.dict_info['Force (pN)'][self.dict_info['index start retract']: self.dict_info['index end retract']][self.idx_peaks_2nd_detection[i]] ) ))
                
                self.dict_info['peaks'].append(self.idx_peaks_2nd_detection[i])
            
            self.tab_peak_detection.grid(row=2, column=0, sticky=W+E+S+N)   
            self.scrollbar.grid(row=2, column=1, sticky="ns")

        

        ax.set_xlabel("Indentation/Separation (nm)")
        ax.set_ylabel("Force (pN)")
        
    def delete_peaks(self):
        """
        This function deletes the peak selected by the user
        from the total list of peaks

        Returns
        -------
        None.

        """
        curItem= self.tab_peak_detection.selection()
        curItems = [(self.tab_peak_detection.item(i)['values']) for i in curItem]
 
        indentation= []
        selected_items = self.tab_peak_detection.selection()
        for i in self.dict_info["peaks"]: 
            indentation.append("{:.2e}".format(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][i]))
        
        for selected_item in selected_items:  
            self.tab_peak_detection.delete(selected_item)
           

        for i in curItems:
            if i[1] in indentation:
                index_peak= indentation.index(i[1])
                indentation.remove(i[1])
                
                self.dict_info["peaks"].pop(index_peak)
        


    def export_data(self):
        """
        This function exports the data

        Returns
        -------
        None.

        """
        
        try:
            self.ListLc
            self.ListLc=sorted(self.ListLc)
        except:
            self.ListLc=None

        self.dict_info['peaks']= sorted(self.dict_info['peaks'])
        
        distance=self.range_before_peak.get()
        d= {'file name': '', 'rupt number':[], 'rupt force (pN)':[], 'rupt time (s)':[], 'rupt sep (nm)':[], 'keff (pN/nm)':[], 'veff (nm/s)':[], 'loading rate (pN/s)':[],
            'vBwd_exp (nm/s)':[], 'peak work (pN* nm)':[]
            }
        #d= dict(self.dict_info)
        for i in range(len(self.dict_info['peaks'])):
            d['rupt number'].append(i+1)
            
            
            d['peak work (pN* nm)'].append(xp.integration(0,
                           self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']][self.dict_info['peaks'][i]],
                           len(self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']])))
        

        
        d['file name']= str(self.tdms_file[-1])
        d['invOLS (nm/V)']= "{:.2e}".format(self.invOLS)
        d['Zsens (nm/V)']="{:.2e}".format(self.sensitivity)
        


        d['Lc (nm)']=["{:.2e}".format(float(i)) for i in self.ListLc]
        
        
        d['keff (pN/nm)'].extend(xp.get_keff_veff_vBwd_exp(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']], 
                                              self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']],
                                              self.dict_info["Deflection (V)"][self.dict_info['index start retract']: self.dict_info['index end retract']],
                                              self.dict_info["time (ms)"][self.dict_info['index start retract']: self.dict_info['index end retract']], 
                                              self.dict_info['peaks'], 
                                              self.dict_info["Distance (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']],
                                              distance)[0])
        
        
        d['veff (nm/s)'].extend(xp.get_keff_veff_vBwd_exp(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']], 
                                              self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']],
                                              self.dict_info["Deflection (V)"][self.dict_info['index start retract']: self.dict_info['index end retract']],
                                              self.dict_info["time (ms)"][self.dict_info['index start retract']: self.dict_info['index end retract']], 
                                              self.dict_info['peaks'], 
                                              self.dict_info["Distance (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']],
                                              distance)[1])
        
        
        d['vBwd_exp (nm/s)'].extend(xp.get_keff_veff_vBwd_exp(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']], 
                                              self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']],
                                              self.dict_info["Deflection (V)"][self.dict_info['index start retract']: self.dict_info['index end retract']],
                                              self.dict_info["time (ms)"][self.dict_info['index start retract']: self.dict_info['index end retract']], 
                                              self.dict_info['peaks'], 
                                              self.dict_info["Distance (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']],
                                              distance)[4])     
        
        d['keff (pN/nm)']= ["{:.2e}".format(i) for i in d['keff (pN/nm)']]
        d['veff (nm/s)']= ["{:.2e}".format(i) for i in d['veff (nm/s)']]
        d['vBwd_exp (nm/s)']= ["{:.2e}".format(i) for i in d['vBwd_exp (nm/s)']]
        
        
        # trigger thres is mininmum of force after point of contact
        d['trigger thres (pN)']= min(self.dict_info["Force (pN)"])
        
        for i in range(len(self.dict_info['peaks'])):
            d['loading rate (pN/s)'].append("{:.2e}".format(self.slope[i]*10**3))
            d['rupt force (pN)'].append("{:.2e}".format(self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']][self.dict_info['peaks'][i]]))
            d['rupt sep (nm)'].append("{:.2e}".format(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][self.dict_info['peaks'][i]]))
            d['rupt time (s)'].append("{:.2e}".format(self.dict_info["time (ms)"][self.dict_info['index start retract']: self.dict_info['index end retract']][self.dict_info['peaks'][i]]*10**-3))
    
        #d['indentation (nm)']= min(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']])
        index_min_force= list(self.dict_info["Force (pN)"][self.dict_info['index start retract']: self.dict_info['index end retract']]).index(d['trigger thres (pN)'])
        
        d['indentation (nm)'] ="{:.2e}".format( abs(self.dict_info["Indentation/Separation (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][index_min_force]))
                        
        
        
        d['kcanti (pN/nm)']= "{:.2e}".format(self.K*10**12*10**-9)
        
        d['vBwd (nm/s)']= "{:.2e}".format(self.dict_info["Distance (nm)"][self.dict_info['index start retract']: self.dict_info['index end retract']][-1]/ (self.dict_info["time (ms)"][-1]*10**-3))
        
        d['trigger thres (pN)']="{:.2e}".format( min(self.dict_info["Force (pN)"]))
        d['dwell time (s)']= "{:.2e}".format(self.dwell*10**-3)


        df = pd.DataFrame.from_dict(d)


        export_file_path = filedialog.asksaveasfilename(defaultextension='.csv')
        

        df.to_csv(export_file_path, sep='\t',header=True, index=False, mode='w')

    




app = App()

app.window.mainloop()
