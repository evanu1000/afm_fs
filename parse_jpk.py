#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 26 12:10:25 2021

@author: Ismahene
"""

import numpy as np
import os
import afmformats

def grab_jpk(directory):
    for file in os.listdir(directory):
        if file.endswith(".jpk-force") and os.path.isfile(os.path.join(directory, file)):
            return os.path.join(directory, file)



def GetJPKFiles(directory):
    """
    This function gets all the tdms files present in the current 
    directory of the user and puts them on a list

    Parameters
    ----------
    directory : str
        The path of the current directory.

    Returns
    -------
    all_tdms_files : list
        a list of all tdms files  .

    """
    all_jpk_files= []
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(".jpk-force"):
              #  print(os.path.join(root, file))
                all_jpk_files.append(os.path.join(root, file))
                
  #  print(all_tdms_files)
    return all_jpk_files


#https://afmformats.readthedocs.io/en/stable/sec_api/afmformats.fmt_jpk.html#module-afmformats.fmt_jpk
def parse_jpk(file):
    """
    

    Parameters
    ----------
    file : str
        jpk-force file.

    Returns
    -------
    force: array
        force in pN.
    time: array
        time in ms .
    height_measured : array
        distance in nm
    height_piezo : array
        extension in nm
    parameters : array 
        array of parms such as spring constant ...etc.
    index_start_approach : int
        index of where the approach starts
    index_end_approach : int
        index of where the approach ends
    index_start_retract : int
        index of where the retract starts
    index_end_retract : int
        index of where the retract ends

    """
 
    dslist= afmformats.fmt_jpk.load_jpk(file)
    
    
    parameters= dslist[0]['metadata']
    data= dslist[0]['data']
 #   print(props(data))

    fdis= afmformats.afm_fdist.AFMForceDistance(data, parameters)
    force_approach= fdis.appr.raw_data['force']
   # force_retract= fdis.retr.data['force']
    indexes_retract=[]
    indexes_approach= []
    for idx, x in np.ndenumerate(fdis.retr.data['segment']):
        if x == True:
            indexes_retract.append(list(idx))
        if x == False:
            indexes_approach.append(list(idx))
        
    index_start_retract=indexes_retract[0][0]
    index_end_retract=indexes_retract[-1][0]
    
    index_start_approach= indexes_approach[0][0]
    index_end_approach= indexes_approach[-1][0]

    dist_retract= fdis.retr.data['segment']


    sensitivity= parameters['sensitivity']
   # print('sensitivity ',sensitivity)
    K= parameters['spring constant']
    
    #dslist = afmformats.formats.load_data(file)

    force= data['force']
    time= data['time']
    height_measured= data['height (measured)'] * 1e9  #convert from  m to nm
    height_piezo= data['height (piezo)']*1e9

    
    #force in pN and time in ms
    return force*1e12, time*1000, height_measured, height_piezo, parameters, index_start_approach, index_end_approach, index_start_retract, index_end_retract

#force, time, height_measured, height_piezo, parameters,index_start_approach, index_end_approach, index_start_retract, index_end_retract= parse_jpk('/Users/macbookair/AFM_FS/force-save-example.jpk-force')
#import matplotlib.pylab as plt

#plt.plot(height_piezo[index_start_approach: index_end_approach], force[index_start_approach: index_end_approach])
#plt.plot(height_piezo[index_start_retract: index_end_retract] , force[index_start_retract: index_end_retract])

#https://afmformats.readthedocs.io/_/downloads/en/latest/pdf/
